<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title>jQuery:  &raquo; jQuery 1.5.1 Released</title>
		<link rel="stylesheet" href="http://static.jquery.com/files/rocker/css/reset.css" type="text/css" />
		<link rel="stylesheet" href="http://static.jquery.com/files/rocker/css/screen.css" type="text/css" />
		<link rel="stylesheet" type="text/css" href="http://blog.jquery.com/css/blog.css" />
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js"></script>
		<script type="text/javascript" src="http://static.jquery.com/files/rocker/scripts/custom.js"></script>
		<link rel="shortcut icon" href="http://static.jquery.com/favicon.ico" type="image/x-icon"/>
		<link rel="search" type="application/opensearchdescription+xml" href="/opensearch_desc.php" title="jQuery JavaScript Library (English)" />
		<link rel="alternate" type="application/rss+xml" title="jQuery Blog" href="http://blog.jquery.com/feed/" />
		<link rel="shortcut icon" href="http://static.jquery.com/favicon.ico" type="image/x-icon"/>
	</head>
<body>


<body id="jq-interior">

	<div id="jq-siteContain">
			<div id="jq-header">
				<a id="jq-siteLogo" href="http://jquery.com/" title="jQuery Home"><img src="http://static.jquery.com/files/rocker/images/logo_jquery_215x53.gif" width="215" height="53" alt="jQuery: Write Less, Do More." /></a>

				<div id="jq-primaryNavigation">

					<ul>
<li class="jq-jquery"><a href="http://jquery.com/" title="jQuery Home">jQuery</a></li>
<li class="jq-plugins"><a href="http://plugins.jquery.com/" title="jQuery Plugins">Plugins</a></li>
<li class="jq-ui"><a href="http://jqueryui.com/" title="jQuery UI">UI</a></li>
<li class="jq-meetup"><a href="http://meetups.jquery.com/" title="jQuery Meetups">Meetups</a></li>
<li class="jq-forum"><a href="http://forum.jquery.com/" title="jQuery Forum">Forum</a></li>
<li class="jq-blog jq-current"><a href="http://blog.jquery.com/" title="jQuery Blog">Blog</a></li>
<li class="jq-about"><a href="http://jquery.org/about" title="About jQuery">About</a></li>
<li class="jq-donate"><a href="http://jquery.org/donate" title="Donate to jQuery">Donate</a></li>
					</ul>
				</div><!-- /#primaryNavigation -->

				<div id="jq-secondaryNavigation">
					<ul>
						<li class="jq-download jq-first"><a href="http://docs.jquery.com/Downloading_jQuery">Download</a></li>
						<li class="jq-documentation"><a href="http://docs.jquery.com/">Documentation</a></li>
						<li class="jq-tutorials"><a href="http://docs.jquery.com/Tutorials">Tutorials</a></li>

						<li class="jq-bugTracker"><a href="http://dev.jquery.com/">Bug Tracker</a></li>
						<li class="jq-discussion jq-last"><a href="http://docs.jquery.com/Discussion">Discussion</a></li>
					</ul>
				</div><!-- /#secondaryNavigation -->

				

				<h1>Blog</h1>

				<form id="jq-primarySearchForm" action="http://blog.jquery.com">

					<div>
						<input type="hidden" value="1" name="ns0"/>
						<label for="primarySearch">Search <span class="jq-jquery">jQuery</span></label>
						<input type="text" value="" name="s" accesskey="f" title="Search jQuery" id="jq-primarySearch"/>
						<button type="submit" name="go" id="jq-searchGoButton"><span>Go</span></button>
					</div>

				</form>

			</div><!-- /#header -->

	<div id="jq-content" class="jq-clearfix">
	
	
	<div id="jq-interiorNavigation">
						<h5>Past Entries</h5>
						<ul>
	<li><a href='http://blog.jquery.com/2011/02/28/jquery-conference-2011-preconference-training/' title='jQuery Conference 2011 &#8211; Preconference Training'>jQuery Conference 2011 &#8211; Preconference Training</a></li>
	<li><a href='http://blog.jquery.com/2011/02/24/jquery-151-released/' title='jQuery 1.5.1 Released'>jQuery 1.5.1 Released</a></li>
	<li><a href='http://blog.jquery.com/2011/02/24/jquery-conference-2011-san-francisco-bay-area-conference-announced/' title='jQuery Conference 2011: San Francisco Bay Area Conference Announced'>jQuery Conference 2011: San Francisco Bay Area Conference Announced</a></li>
	<li><a href='http://blog.jquery.com/2011/02/18/jquery-1-5-1-rc-1-released/' title='jQuery 1.5.1 RC 1 Released'>jQuery 1.5.1 RC 1 Released</a></li>
	<li><a href='http://blog.jquery.com/2011/02/07/jqcommunity-updates-feb2011/' title='New Releases, Videos &amp; A Sneak Peek At The jQuery UI Grid'>New Releases, Videos &#038; A Sneak Peek At The jQuery UI Grid</a></li>
	<li><a href='http://blog.jquery.com/2011/01/31/jquery-15-released/' title='jQuery 1.5 Released'>jQuery 1.5 Released</a></li>
	<li><a href='http://blog.jquery.com/2011/01/30/api-documentation-changes/' title='API Documentation Changes'>API Documentation Changes</a></li>
	<li><a href='http://blog.jquery.com/2011/01/24/jquery-15rc-1-released/' title='jQuery 1.5 RC 1 Released'>jQuery 1.5 RC 1 Released</a></li>
	<li><a href='http://blog.jquery.com/2011/01/14/jquery-1-5-beta-1-released/' title='jQuery 1.5 Beta 1 Released'>jQuery 1.5 Beta 1 Released</a></li>
	<li><a href='http://blog.jquery.com/2010/12/30/hotlinking-to-be-disabled-on-jan-31-2011/' title='Hotlinking to be disabled on January 31, 2011'>Hotlinking to be disabled on January 31, 2011</a></li>
	<li><a href='http://blog.jquery.com/2010/12/28/jquery-community-updates-for-december-2010/' title='jQuery Community Updates For December 2010'>jQuery Community Updates For December 2010</a></li>
	<li><a href='http://blog.jquery.com/2010/11/23/jquery-community-updates-november2010/' title='jQuery Community Updates For November 2010'>jQuery Community Updates For November 2010</a></li>
	<li><a href='http://blog.jquery.com/2010/11/23/team-spotlight-the-jquery-bug-triage-team/' title='Team Spotlight: The jQuery Bug Triage Team'>Team Spotlight: The jQuery Bug Triage Team</a></li>
	<li><a href='http://blog.jquery.com/2010/11/11/jquery-1-4-4-release-notes/' title='jQuery 1.4.4 Released'>jQuery 1.4.4 Released</a></li>
	<li><a href='http://blog.jquery.com/2010/11/03/jquery-1-4-4-release-candidate-2-released/' title='jQuery 1.4.4 Release Candidate 2 Released'>jQuery 1.4.4 Release Candidate 2 Released</a></li>
						</ul>
				</div><!-- /#interiorNavigation -->	
	<div id="jq-primaryContent">
				
  	
		<div class="post" id="post-1245">
			<h2><a href="/">Blog</a> &raquo; jQuery 1.5.1 Released</h2>
<small>Posted February 24th, 2011 by John Resig</small>
	
			<div class="entrytext">
				<p>jQuery 1.5.1 is now out! This is the first minor release on top of jQuery 1.5 and lands a number of fixes for bugs.</p>
<p>We would like to thank the following contributors that provided patches towards this release: antonkovalyov, csnover, danheberden, davidmurdoch, dmethvin, gnarf37, jaubourg, jeresig, jitter, jrburke, lrbabe, mathiasbynens, rwldrn, SlexAxton, and voxwerk.</p>
<p>We&#8217;d especially like to thank our bug triage team who assisted in narrowing down some of the important fixes needed for this release.</p>
<h3>Downloading</h3>
<p>As usual, we provide two copies of jQuery, one minified and one uncompressed (for debugging or reading).</p>
<ul>
<li><a href="http://code.jquery.com/jquery-1.5.1.min.js">jQuery Minified</a> (29kb <a href="http://www.julienlecomte.net/blog/2007/08/13/">Gzipped</a>)</li>
<li><a href="http://code.jquery.com/jquery-1.5.1.js">jQuery Regular</a> (212kb)</li>
</ul>
<p>You can feel free to include the above URLs directly into your site  and you will get the full performance benefits of a quickly-loading  jQuery.</p>
<p>Additionally you can also load the URLs directly from Microsoft and Google&#8217;s CDNs:</p>
<p>Microsoft CDN: <a href="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.5.1.min.js">http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.5.1.min.js</a></p>
<p>Google CDN: <a href="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js">https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js</a></p>
<h3>1.6 Roadmap Meeting</h3>
<p>The core jQuery dev team is now taking proposals for changes to land in jQuery 1.6. Right now we&#8217;re shooting to release jQuery 1.6 late April/early May and will be meeting to discuss the submitted proposals at Noon EST on March 7th (the meeting will be in #jquery-meeting on irc.freenode.net).</p>
<p>Please submit your proposals through the <a href="https://spreadsheets.google.com/viewform?formkey=dDcwNUlSaWltOWJrNE5tOUlIbkVJbGc6MQ#gid=0">following form</a> in advance of the March 7th meeting.</p>
<h3>jQuery 1.5.1 Changelog</h3>
<p><center><a href="http://www.flickr.com/photos/jeresig/5474135253/" title="jQuery 1.5.1 Test Swarm Results by John Resig, on Flickr"><img src="http://farm6.static.flickr.com/5136/5474135253_47e9d7097b.jpg" width="500" height="163" alt="jQuery 1.5.1 Test Swarm Results" /></a></center></p>
<p>API Documentation: <a href="http://api.jquery.com/category/version/1.5.1/">jQuery 1.5.1 API Documentation</a></p>
<p>As this was a bug fix release there are no new features to report upon. The only changes are as follows:</p>
<p>jQuery now supports Internet Explorer 9 as a top level browser. All known bugs have been fixed and/or been reported to the IE team for resolution in the final release.</p>
<p>Three new options were added to <a href="http://api.jquery.com/jQuery.ajax/">jQuery.ajax()</a>:</p>
<ol>
<li><strong>isLocal:</strong> Allow the current environment to be recognized as &#8220;local,&#8221; (e.g. the filesystem), even if jQuery does not recognize it as such by default. The following protocols are currently recognized as local: file, *-extension, and widget. If the isLocal setting needs modification, it is recommended to do so once in the $.ajaxSetup() method.</li>
<li><strong>mimeType:</strong> A mime type to override the XHR mime type.</li>
<li><strong>xhrFields</strong> A map of fieldName-fieldValue pairs to set on the native XHR object. For example, you can use it to set withCredentials to true for cross-domain requests if needed.</li>
</ol>
<p><br/></p>
<h3>Closed Tickets</h3>
<p>A full list of all bugs and tickets closed are as follows:</p>
<ul>
<li><a href="http://bugs.jquery.com/ticket/2551">#2551</a> Make sure .val() works after form.reset() in IE</li>
<li><a href="http://bugs.jquery.com/ticket/4537">#4537</a> Make sure .clone(true) correctly clones namespaced events</li>
<li><a href="http://bugs.jquery.com/ticket/4966">#4966</a> Don&#8217;t add &#8220;px&#8221; to unit-less properties when animating them</li>
<li><a href="http://bugs.jquery.com/ticket/6774">#6774</a> Make sure we only access parentNode if it&#8217;s available. Fixes an issue where after an option tag has been detached, an elem.parentNode error would be thrown.</li>
<li><a href="http://bugs.jquery.com/ticket/6911">#6911</a>Prevent action on disabled elements, both triggering and bound via .live() </li>
<li><a href="http://bugs.jquery.com/ticket/7531">#7531</a> Fix again for IE9RC. Enhances ajaxSetup so that it can take an optional target option, in which case target will be updated instead of ajaxSettings. That way, fields that shouldn&#8217;t be deep extended can be listed and dealt with in one place. jQuery.ajax now makes use of ajaxSetup with target to create its internal settings object</li>
<li><a href="http://bugs.jquery.com/ticket/7568">#7568</a> Follow-up fix for <a href="http://bugs.jquery.com/ticket/5862">#5862</a>. Objects with a length property weren&#8217;t serialized properly by jQuery.param</li>
<li><a href="http://bugs.jquery.com/ticket/7653">#7653</a> Changes regexp to detect local protocol so that it will accept any protocol finishing by -extension</li>
<li><a href="http://bugs.jquery.com/ticket/7668">#7668</a> Sizzle and jQuery QUnit tests are out of sync</li>
<li><a href="http://bugs.jquery.com/ticket/7912">#7912</a> This change makes .cur() more .cssHooks friendly. .cur() now returns the unmodified value by .css() if it isn&#8217;t a number, number-alike or a value that needs a fallback to 0.</li>
<li><a href="http://bugs.jquery.com/ticket/7922">#7922</a> Fixed an issue where live(&#8216;click&#8217;) doesn&#8217;t fire when live(&#8216;submit&#8217;) is bound first in IE</li>
<li><a href="http://bugs.jquery.com/ticket/7945">#7945</a> Make jQuery.param() serialize plain objects with a property named jquery correctly</li>
<li><a href="http://bugs.jquery.com/ticket/8033">#8033</a> jQuery 1.4.4+ fails to load on pages with old Prototype (<= 1.5) or Current Prototype + Scriptaculous in IE</li>
<li><a href="http://bugs.jquery.com/ticket/8039">#8039</a> Selectors with HTML5 input types not work in IE6/7</li>
<li><a href="http://bugs.jquery.com/ticket/8052">#8052</a> Update jQuery.support.noCloneEvent test to function properly in IE9</li>
<li><a href="http://bugs.jquery.com/ticket/8095">#8095</a> Properly handles the case where browser cache needs to be bypassed while server-side logic still delivers proper 304 responses. Unit test added</li>
<li><a href="http://bugs.jquery.com/ticket/8098">#8098</a> Use the fast document.head when available</li>
<li><a href="http://bugs.jquery.com/ticket/8099">#8099</a> Always restore to correct display value based on element&#8217;s expected default display</li>
<li><a href="http://bugs.jquery.com/ticket/8107">#8107</a> Fix argument handling for $.ajax for multiple method signatues and add test case</li>
<li><a href="http://bugs.jquery.com/ticket/8108">#8108</a>Temporary fix for jQuery metadata being exposed on plain JS objects when serializing with JSON.stringify to avoid compatibility-breaking changes. A proper fix for this will be landed in 1.6</li>
<li><a href="http://bugs.jquery.com/ticket/8115">#8115</a> Renames all references to jXHR with jqXHR in the code (like was done in the doc)</li>
<li><a href="http://bugs.jquery.com/ticket/8123">#8123</a> The default for .clone() is to not clone any events</li>
<li><a href="http://bugs.jquery.com/ticket/8125">#8125</a> Status is set to 200 for requests with status 0 when location.protocol if &#8220;file:&#8221;. Added test/localfile.html to control it works</li>
<li><a href="http://bugs.jquery.com/ticket/8129">#8129</a> Fix cloning multiple selected options in IE8</li>
<li><a href="http://bugs.jquery.com/ticket/8135">#8135</a> Makes sure any exception thrown by Firefox when trying to access an XMLHttpRequest property when a network error occured is caught and notified as an error. Added test/networkerror.html to test the behavior</li>
<li><a href="http://bugs.jquery.com/ticket/8138">#8138</a> Access to document.location is made only once at load time and if it fails (throwing an exception in IE when document.domain is already set), we use the href of an A element instead</li>
<li><a href="http://bugs.jquery.com/ticket/8145">#8145</a> Added readyWait tests</li>
<li><a href="http://bugs.jquery.com/ticket/8146">#8146</a> introducing the xhrFields option with is a map of fieldName/fieldValue to set on the native xhr. Can be used to set withCredentials to true for cross-domain requests if needed</li>
<li><a href="http://bugs.jquery.com/ticket/8152">#8152</a> applying the same special cases for protocol &#8220;chrome-extension:&#8221; as were for &#8220;file:&#8221; (needs tests). Re-organizes and fixes the handling of special cases for HTTP status code in the xhr transport</li>
<li><a href="http://bugs.jquery.com/ticket/8177">#8177</a> XHR transport now considers 304 Not Modified responses as 200 OK if no conditional request header was provided (as per the XMLHttpRequest specification)</li>
<li><a href="http://bugs.jquery.com/ticket/8193">#8193</a> Fixes abort in prefilter. No global event will be fired in that case even if the global option is set to true. Unit test added</li>
<li><a href="http://bugs.jquery.com/ticket/8198">#8198</a> Remove unnecessary &#8220;script.type = text/javascript;&#8221;</li>
<li><a href="http://bugs.jquery.com/ticket/8200">#8200</a> Unexpose $.support._scriptEval as it&#8217;s not needed. Use a private var instead</li>
<li><a href="http://bugs.jquery.com/ticket/8209">#8209</a> Make sure that mousing over Chrome &#8220;internal div&#8221; doesn&#8217;t trigger a mouseleave</li>
<li><a href="http://bugs.jquery.com/ticket/8219">#8219</a> Introduces the mimeType option to override content-type header in conversion (and in native xhr when possible). Adds companion overrideMimeType method on jqXHR object (it simply sets the option)</li>
<li><a href="http://bugs.jquery.com/ticket/8220">#8220</a> Remove backslashes from tag name filter</li>
<li><a href="http://bugs.jquery.com/ticket/8245">#8245</a> Ajax now ensures header names are capitalized so that non-compliant xhr implementations don&#8217;t override them</li>
<li><a href="http://bugs.jquery.com/ticket/8250">#8250</a> ajax does not work in opera 10 widgets</li>
<li><a href="http://bugs.jquery.com/ticket/8277">#8277</a> Sets data to undefined rather than null when it is not provided in ajax helpers so that it won&#8217;t revent data set in ajaxSettings from being used. </li>
<li><a href="http://bugs.jquery.com/ticket/8297">#8297</a> Make sure response headers with empty values are handled properly and do not prevent proper parsing of the entire response headers string.</li>
<li><a href="http://bugs.jquery.com/ticket/8353">#8353</a> Adds a catch block in resolveWith so that the finally block gets executed in IE7 and IE6.</li>
<li><a href="http://bugs.jquery.com/ticket/8365">#8365</a> Make sure that IE 9 still clones attributes.</li>
</ul>
	
				
<hr/>
	
			</div>
		</div>
		
	
<!-- You can start editing here. -->

	<h3 id="comments">17 Responses to &#8220;jQuery 1.5.1 Released&#8221;</h3> 

	<ol class="commentlist">

	
		<li class="alt" id="comment-525698">
			<cite>Tom</cite> Says:
						<br />

			<small class="commentmetadata"><a href="#comment-525698" title="">February 24th, 2011 at 3:50 pm</a> </small>

			<p>When is the Google cdn version released?</p>

		</li>

	
	
		<li class="" id="comment-525699">
			<cite><a href='http://www.stookstudio.com' rel='external nofollow' class='url'>Erwin Heiser</a></cite> Says:
						<br />

			<small class="commentmetadata"><a href="#comment-525699" title="">February 24th, 2011 at 3:55 pm</a> </small>

			<p>Google CDN version is giving a &#8220;not found&#8221; error.</p>

		</li>

	
	
		<li class="alt" id="comment-525700">
			<cite><a href='http://ejohn.org/' rel='external nofollow' class='url'>John Resig</a></cite> Says:
						<br />

			<small class="commentmetadata"><a href="#comment-525700" title="">February 24th, 2011 at 4:04 pm</a> </small>

			<p>@All: The Google CDN stuff should be up soon, I&#8217;ve already contacted the Google folks and they&#8217;re working on it.</p>

		</li>

	
	
		<li class="" id="comment-525701">
			<cite><a href='http://alexsexton.com/' rel='external nofollow' class='url'>Alex Sexton</a></cite> Says:
						<br />

			<small class="commentmetadata"><a href="#comment-525701" title="">February 24th, 2011 at 4:12 pm</a> </small>

			<p>There was also a temporary fix to #8018 &#8211; if anyone is interested in that bug.</p>

		</li>

	
	
		<li class="alt" id="comment-525702">
			<cite><a href='http://www.scheible.eu' rel='external nofollow' class='url'>Tobias Scheible</a></cite> Says:
						<br />

			<small class="commentmetadata"><a href="#comment-525702" title="">February 24th, 2011 at 5:41 pm</a> </small>

			<p>The Google CDN works now</p>

		</li>

	
	
		<li class="" id="comment-525704">
			<cite>StevenS</cite> Says:
						<br />

			<small class="commentmetadata"><a href="#comment-525704" title="">February 24th, 2011 at 11:45 pm</a> </small>

			<p>Not to nitpick, but <a href="http://docs.jquery.com/Downloading_jQuery" rel="nofollow">http://docs.jquery.com/Downloading_jQuery</a> still has ajax.microsoft.com instead of ajax.aspnetcdn.com for the domain. I know Microsoft is trying to depreciate use of the ajax.microsoft domain in favor of the aspnetcdn. Other than that, many fixes in this are welcome!</p>

		</li>

	
	
		<li class="alt" id="comment-525705">
			<cite><a href='http://myblog.sunfarms.net' rel='external nofollow' class='url'>Wendal</a></cite> Says:
						<br />

			<small class="commentmetadata"><a href="#comment-525705" title="">February 24th, 2011 at 11:45 pm</a> </small>

			<p>Great!! Thanks a lot.</p>

		</li>

	
	
		<li class="" id="comment-525709">
			<cite><a href='http://www.kanema.com.br/browsers/eudora.php' rel='external nofollow' class='url'>Eduardo Pacheco</a></cite> Says:
						<br />

			<small class="commentmetadata"><a href="#comment-525709" title="">February 25th, 2011 at 8:36 am</a> </small>

			<p>Nice! Good job!</p>

		</li>

	
	
		<li class="alt" id="comment-525710">
			<cite>Pcmanik</cite> Says:
						<br />

			<small class="commentmetadata"><a href="#comment-525710" title="">February 25th, 2011 at 8:58 am</a> </small>

			<p>You have to change jQuery version and download on the main page.</p>

		</li>

	
	
		<li class="" id="comment-525712">
			<cite><a href='http://www.eboyer.com' rel='external nofollow' class='url'>Eroan</a></cite> Says:
						<br />

			<small class="commentmetadata"><a href="#comment-525712" title="">February 25th, 2011 at 10:51 am</a> </small>

			<p>Thanks for this new release to all the community !</p>

		</li>

	
	
		<li class="alt" id="comment-525714">
			<cite><a href='http://mooseman.s5.com' rel='external nofollow' class='url'>Mooseman</a></cite> Says:
						<br />

			<small class="commentmetadata"><a href="#comment-525714" title="">February 25th, 2011 at 6:34 pm</a> </small>

			<p>Great! Happy to see more compatability for the infamous Internet Explorer. :)</p>

		</li>

	
	
		<li class="" id="comment-525715">
			<cite>Ruon</cite> Says:
						<br />

			<small class="commentmetadata"><a href="#comment-525715" title="">February 25th, 2011 at 10:44 pm</a> </small>

			<p>CDN&#8217;s are a bad idea. Here in the third world, where Internet crawls at the speed of turtle, DNS lookups can take a ridiculously long time. So when third-party DNS lookups i.e. to CDNs are called from a website, pretty much all this achieves is stalling the website from loading, sometimes completely. And in the West, where Internet is fast, what difference does loading 30Kb locally make? Durr&#8230;</p>

		</li>

	
	
		<li class="alt" id="comment-525731">
			<cite>KevinC</cite> Says:
						<br />

			<small class="commentmetadata"><a href="#comment-525731" title="">February 27th, 2011 at 3:15 pm</a> </small>

			<p>I&#8217;ve been getting this in Firebug with the latest version of Firefox on any script I load using jquery1.5 and 1.5.1</p>
<p>Unexpected token in attribute selector: &#8216;!&#8217;.</p>

		</li>

	
	
		<li class="" id="comment-525737">
			<cite>Carson</cite> Says:
						<br />

			<small class="commentmetadata"><a href="#comment-525737" title="">March 1st, 2011 at 10:10 am</a> </small>

			<p>The point for CDN is that when someone else has downloaded the copies from another CDN&#8217;s site. the browser doesn&#8217;t need to download again for your site. Google CDN is so famous that it can help many people in this way. And some google services even use their own CDN&#8217;s jquery that you may not discover that you use it frequently actually</p>

		</li>

	
	
		<li class="alt" id="comment-525739">
			<cite><a href='http://qqq' rel='external nofollow' class='url'>?</a></cite> Says:
						<br />

			<small class="commentmetadata"><a href="#comment-525739" title="">March 3rd, 2011 at 3:31 am</a> </small>

			<p>qqq</p>

		</li>

	
	
		<li class="" id="comment-525740">
			<cite><a href='http://qqq' rel='external nofollow' class='url'>plly</a></cite> Says:
						<br />

			<small class="commentmetadata"><a href="#comment-525740" title="">March 3rd, 2011 at 3:31 am</a> </small>

			<p>I&#8217;m china</p>

		</li>

	
	
		<li class="alt" id="comment-525742">
			<cite><a href='http://www.spiele-power.de' rel='external nofollow' class='url'>Phantom</a></cite> Says:
						<br />

			<small class="commentmetadata"><a href="#comment-525742" title="">March 3rd, 2011 at 7:34 am</a> </small>

			<p>wow, the new version came fast. i love jQuery and use it on my website for a star rating vote :)</p>

		</li>

	
	
	</ol>

 


<h3 id="respond">Leave a Reply</h3>


<form action="http://blog.jquery.com/wp-comments-post.php" method="post" id="commentform">


<p><input type="text" name="author" id="author" value="" size="22" tabindex="1" />
<label for="author"><small>Name (required)</small></label></p>

<p><input type="text" name="email" id="email" value="" size="22" tabindex="2" />
<label for="email"><small>Mail (will not be published) (required)</small></label></p>

<p><input type="text" name="url" id="url" value="" size="22" tabindex="3" />
<label for="url"><small>Website</small></label></p>


<!--<p><small><strong>XHTML:</strong> You can use these tags: &lt;a href=&quot;&quot; title=&quot;&quot;&gt; &lt;abbr title=&quot;&quot;&gt; &lt;acronym title=&quot;&quot;&gt; &lt;b&gt; &lt;blockquote cite=&quot;&quot;&gt; &lt;cite&gt; &lt;code&gt; &lt;del datetime=&quot;&quot;&gt; &lt;em&gt; &lt;i&gt; &lt;q cite=&quot;&quot;&gt; &lt;strike&gt; &lt;strong&gt; </small></p>-->

<p><textarea name="comment" id="comment" rows="10" tabindex="4" style="width:100%"></textarea></p>

<p><input name="submit" type="submit" id="submit" tabindex="5" value="Submit Comment" />
<input type="hidden" name="comment_post_ID" value="1245" />
</p>


<!-- This site's performance optimized by W3 Total Cache. Dramatically improve the speed and reliability of your blog!

Learn more about our WordPress Plugins: http://www.w3-edge.com/wordpress-plugins/

Page Caching using disk

Served from: blog.jquery.com @ 2011-03-06 11:19:24 -->