/***************************************************
			SuperFish Menu
***************************************************/	
// initialise plugins
	jQuery.noConflict()(function(){
		jQuery('ul#menu').superfish();
	});
	
	
	
jQuery.noConflict()(function($) {
  if ($.browser.msie && $.browser.version.substr(0,1)<7)
  {
	$('li').has('ul').mouseover(function(){
		$(this).children('ul').css('visibility','visible');
		}).mouseout(function(){
		$(this).children('ul').css('visibility','hidden');
		})
  }
}); 

jQuery.noConflict()(function(){
	// Setup Slider
	jQuery(".my_asyncslider").asyncSlider({
        autoswitch: 10 * 1000,
        minTime: 1500,
        maxTime: 2000
    });

});

/***************************************************
			TWITTER FEED
***************************************************/


jQuery.noConflict()(function($){
  $(document).ready(function(){
		$('#login-trigger').click(function(){
			$(this).next('#login-content').slideToggle();
			$(this).toggleClass('active');					
			
			if ($(this).hasClass('active')) $(this).find('span').html('&#x25B2;')
				else $(this).find('span').html('&#x25BC;')
			})
  });
});


jQuery.noConflict()(function($){
$(document).ready(function() {
	$('ul#filter a').click(function() {
		$(this).css('outline','none');
		$('ul#filter .current').removeClass('current');
		$(this).parent().addClass('current');
		
		var filterVal = $(this).text().toLowerCase().replace(' ','-');
				
		if(filterVal == 'all') {
			$('ul#portfolio li.hidden').fadeIn('slow').removeClass('hidden');
		} else {
			
			$('ul#portfolio li').each(function() {
				if(!$(this).hasClass(filterVal)) {
					$(this).fadeOut('normal').addClass('hidden');
				} else {
					$(this).fadeIn('slow').removeClass('hidden');
				}
			});
		}

		return false;
	});
});
});



/***************************************************
 Slider "4to mu ymeem"
 ***************************************************/

jQuery(document).ready(function(){
    function htmSlider(){
        /* ������� ��������� ���������� */
        /* ������� �������� */
        var slideWrap = jQuery('.slide-wrap');
        /* ������ �� ���������� ���������� ����� */
        var nextLink = jQuery('#img_right');
        var prevLink = jQuery('#img_left');
        var playLink = jQuery('.auto');
        var is_animate = false;
        /* ������ ������ � ��������� */
        var slideWidth = jQuery('.slide-item').outerWidth();
        /* �������� �������� */
        var newLeftPos = slideWrap.position().left - slideWidth;
        /* ���� �� ������ �� ��������� ����� */
        nextLink.click(function(){
            if(!slideWrap.is(':animated')) {
                slideWrap.animate({left: newLeftPos}, 900, function(){
                    slideWrap
                        .find('.slide-item:first')
                        .appendTo(slideWrap)
                        .parent()
                        .css({'left': 0});
                });
            }
        });
        /* ���� �� ������ �� ����������� ����� */
        prevLink.click(function(){
            if(!slideWrap.is(':animated')) {
                slideWrap
                    .css({'left': newLeftPos})
                    .find('.slide-item:last')
                    .prependTo(slideWrap)
                    .parent()
                    .animate({left: 0}, 900);
            }
        });
        /* ������� �������������� ��������� �������� */
        function autoplay(){
            if(!is_animate){
                is_animate = true;
                slideWrap.animate({left: newLeftPos}, 500, function(){
                    slideWrap
                        .find('.slide-item:first')
                        .appendTo(slideWrap)
                        .parent()
                        .css({'left': 0});
                    is_animate = false;
                });
            }
        }
        /* ����� �� ������� �����/����� */
        playLink.click(function(){
            if(playLink.hasClass('play')){
                playLink.removeClass('play').addClass('pause');
                jQuery('.navy').addClass('disable');
                timer = setInterval(autoplay, 1000);
            } else {
                playLink.removeClass('pause').addClass('play');
                jQuery('.navy').removeClass('disable');
                clearInterval(timer);
            }
        });
    }
    /* ������������� ������� �������� */
    htmSlider();
});



