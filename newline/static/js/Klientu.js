/**
 * Created with JetBrains PhpStorm.
 * User: Юля
 * Date: 06.06.13
 * Time: 13:13
 * To change this template use File | Settings | File Templates.
 */
/***************************************************
 Slider "Clientu SPEAK"
 ***************************************************/


jQuery(document).ready(function(){
    function htmSlider(){
        /* ������� ��������� ���������� */
        /* ������� �������� */
        var slideWrap = jQuery('.slide-wrapSpeak');
        /* ������ �� ���������� ���������� ����� */
        var nextLink = jQuery('#next');
        var prevLink = jQuery('#prev');
        var playLink = jQuery('.auto');
        var is_animate = false;
        /* ������ ������ � ��������� */
        var slideWidth = jQuery('.slide-itemSpeak').outerWidth();
        /* �������� �������� */
        var newLeftPos = slideWrap.position().left - slideWidth;
        /* ���� �� ������ �� ��������� ����� */
        nextLink.click(function(){
            if(!slideWrap.is(':animated')) {
                slideWrap.animate({left: newLeftPos}, 900, function(){
                    slideWrap
                        .find('.slide-itemSpeak:first')
                        .appendTo(slideWrap)
                        .parent()
                        .css({'left': 0});
                });
            }
        });
        /* ���� �� ������ �� ����������� ����� */
        prevLink.click(function(){
            if(!slideWrap.is(':animated')) {
                slideWrap
                    .css({'left': newLeftPos})
                    .find('.slide-itemSpeak:last')
                    .prependTo(slideWrap)
                    .parent()
                    .animate({left: 0}, 900);
            }
        });
        /* ������� �������������� ��������� �������� */
        function autoplay(){
            if(!is_animate){
                is_animate = true;
                slideWrap.animate({left: newLeftPos}, 500, function(){
                    slideWrap
                        .find('.slide-itemSpeak:first')
                        .appendTo(slideWrap)
                        .parent()
                        .css({'left': 0});
                    is_animate = false;
                });
            }
        }
        /* ����� �� ������� �����/����� */

    }
    /* ������������� ������� �������� */
    htmSlider();
});
