$(document).ready(function(){
    $('textarea#id_features').hide();
    ul = $('<ul id=sortable></ul>');

    for (x in $('textarea#id_features').val().split(','))
    {
        $(ul).append($('<li class="ui-state-default">'+$('textarea#id_features').val().split(',')[x]+'</li>'))
    }
    $(ul).insertAfter($('textarea#id_features'));
    $( "#sortable" ).sortable({
        placeholder: "ui-state-highlight"
    });
    $( "#sortable" ).disableSelection();
});