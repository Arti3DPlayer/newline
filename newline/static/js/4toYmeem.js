
jQuery(document).ready(function(){
    function htmSlider(){
        /* ������� ��������� ���������� */
        /* ������� �������� */
        var slideWrap = jQuery('.slide-wrap');
        /* ������ �� ���������� ���������� ����� */
        var nextLink = jQuery('#img_right');
        var prevLink = jQuery('#img_left');
        var playLink = jQuery('.auto');
        var is_animate = false;
        /* ������ ������ � ��������� */
        var slideWidth = jQuery('.slide-item').outerWidth();
        /* �������� �������� */
        var newLeftPos = slideWrap.position().left - slideWidth;
        /* ���� �� ������ �� ��������� ����� */
        nextLink.click(function(){
            if(!slideWrap.is(':animated')) {
                slideWrap.animate({left: newLeftPos}, 900, function(){
                    slideWrap
                        .find('.slide-item:first')
                        .appendTo(slideWrap)
                        .parent()
                        .css({'left': 0});
                });
            }
        });
        /* ���� �� ������ �� ����������� ����� */
        prevLink.click(function(){
            if(!slideWrap.is(':animated')) {
                slideWrap
                    .css({'left': newLeftPos})
                    .find('.slide-item:last')
                    .prependTo(slideWrap)
                    .parent()
                    .animate({left: 0}, 900);
            }
        });
        /* ������� �������������� ��������� �������� */
        function autoplay(){
            if(!is_animate){
                is_animate = true;
                slideWrap.animate({left: newLeftPos}, 500, function(){
                    slideWrap
                        .find('.slide-item:first')
                        .appendTo(slideWrap)
                        .parent()
                        .css({'left': 0});
                    is_animate = false;
                });
            }
        }
        /* ����� �� ������� �����/����� */
        playLink.click(function(){
            if(playLink.hasClass('play')){
                playLink.removeClass('play').addClass('pause');
                jQuery('.navy').addClass('disable');
                timer = setInterval(autoplay, 1000);
            } else {
                playLink.removeClass('pause').addClass('play');
                jQuery('.navy').removeClass('disable');
                clearInterval(timer);
            }
        });
    }
    /* ������������� ������� �������� */
    htmSlider();
});
/**
 * Created with JetBrains PhpStorm.
 * User: Юля
 * Date: 05.06.13
 * Time: 18:48
 * To change this template use File | Settings | File Templates.
 */
