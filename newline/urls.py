from django.conf.urls import patterns, include, url
from django.conf import settings
from django.views.generic.base import RedirectView

from newline.main.views import *

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
	url(r'^$', RedirectView.as_view(url='/home/')),
    url(r'^home/$', home, name="home"),
    url(r'^portfolio/$', portfolio, name="portfolio"),
    url(r'^portfolio/(?P<page>\d+)/$', portfolio_details, name="portfolio_details"),
    url(r'^contacts/$', contacts, name="contacts"),
    url(r'^about/$', about, name="about"),
    url(r'^about/blog/$', blog, name="blog"),
    url(r'^about/technology/$', technology, name="technology"),
    url(r'^services/$', services, name="services"),

    (r'^tinymce/', include('tinymce.urls')),
    url(r'^lang/(?P<code>[a-z]{2})/$', lang, name='lang'),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^admin_tools/', include('admin_tools.urls')),
    url(r'^admin/', include(admin.site.urls)),
)


if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT,}),
   )