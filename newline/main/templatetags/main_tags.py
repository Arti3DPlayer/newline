# -*- coding: utf-8 -*-
from django.conf import settings
from django import template
from django.utils.translation import ugettext_lazy as _
from django.contrib.flatpages.models import FlatPage
from django.db.models import Q, F, Avg, Count

from newline.main.models import SEO

register = template.Library()

@register.inclusion_tag('inclusion/seo.html', takes_context=True)
def seo_tag(context):
    request = context['request']
    try:
        s = SEO.objects.get(url=request.path)
        return {'seo': s}
    except:
        print 'Warning: No seo tags for this page'