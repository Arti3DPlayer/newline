# -*- coding: utf-8 -*-
from modeltranslation.translator import translator, TranslationOptions
from newline.main.models import Portfolio, News, FeedbackAboutUs, Technology, SubTechnology, WhatWeCanDo, SEO, MainSlider, Services, Contacts

from django.contrib.flatpages.models import FlatPage

class ContactsTranslationOptions(TranslationOptions):
    fields = ('title', 'info_block1', 'info_block2', 'info_block3', 'info_block4' )

class ServicesTranslationOptions(TranslationOptions):
    fields = ('title', 'description')

class MainSliderTranslationOptions(TranslationOptions):
    fields = ('title', 'description')

class SEOTranslationOptions(TranslationOptions):
    fields = ('title', 'description', 'keywords',)

class WhatWeCanDoTranslationOptions(TranslationOptions):
    fields = ('text',)

class FlatPageTranslationOptions(TranslationOptions):
    fields = ('title', 'content',)

class PortfolioTranslationOptions(TranslationOptions):
    fields = ('title', 'description','features',)

class NewsTranslationOptions(TranslationOptions):
    fields = ('title', 'description','text',)

class FeedbackAboutUsTranslationOptions(TranslationOptions):
    fields = ('author', 'about_author','feedback',)

class TechnologyTranslationOptions(TranslationOptions):
    fields = ('title', 'description',)

class SubTechnologyTranslationOptions(TranslationOptions):
    fields = ('title', 'description',)

translator.register(Contacts, ContactsTranslationOptions)
translator.register(Services, ServicesTranslationOptions)
translator.register(MainSlider, MainSliderTranslationOptions)
translator.register(WhatWeCanDo, WhatWeCanDoTranslationOptions)
translator.register(SEO, SEOTranslationOptions)
translator.register(Technology, TechnologyTranslationOptions)
translator.register(SubTechnology, SubTechnologyTranslationOptions)
translator.register(FeedbackAboutUs, FeedbackAboutUsTranslationOptions)
translator.register(News, NewsTranslationOptions)
translator.register(Portfolio, PortfolioTranslationOptions)
translator.register(FlatPage, FlatPageTranslationOptions)