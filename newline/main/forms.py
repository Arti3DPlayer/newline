# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import ugettext as _

class ContactForm(forms.Form):
    name = forms.CharField(label=_(u"Имя"),max_length=30,widget = forms.TextInput(attrs={"class":"text","placeholder":_(u"Имя")} ),)
    email = forms.EmailField(label=_(u"E-mail"),widget = forms.TextInput(attrs={"class":"text","placeholder":_(u"E-mail")} ),)
    phone = forms.CharField(widget = forms.TextInput(attrs={"class":"texts","placeholder":_(u"Телефон")} ),)
    subject = forms.CharField(max_length=100,widget = forms.TextInput(attrs={"class":"texts","placeholder":_(u"Тема")} ),)
    message = forms.CharField(widget=forms.Textarea(attrs={"class":"text","placeholder":_(u"Сообщение")} ))