# -*- coding: utf-8 -*-
from django.db import models
from tinymce.models import HTMLField
from stdimage import StdImageField
from south.modelsinspector import add_introspection_rules

PORTFOLIO_TYPE_CHOICES = (
    ('1', 'Проекты под ключ'),
    ('2', 'Web Дизайн'),
    ('3', 'Стили и Лого'),
    ('4', 'SEO'),
)
#rules for south and stdimage
rules = [
            (
                (StdImageField, ), [],
                {
                    "upload_to" : [ "upload_to" , { "default" : None}],
                    "height_field" : [ "height_field" , { "default" : None}],
                    "width_field" : [ "width_field" , { "default" : None}],
                    "null" : [ "null" , { "default" : False }],
                    "blank" : [ "blank" , { "default" : False }],
                    "max_length" : [ "max_length" , { "default" : 100}],
                    "size": ["size", {"default": None}],
                    "thumbnail_size": ["thumbnail_size",{"default": None}],
                }
            ),
        ]
add_introspection_rules(rules, ["^stdimage\.fields",]) 



class Services(models.Model):
    title = HTMLField(max_length=200, verbose_name=u'Заголовок')
    description = HTMLField(verbose_name=u'Описание')
    priority =  models.IntegerField(default=1,blank = True, null = True, verbose_name=u'Приоритет') 

    class Meta:
        verbose_name = u'Услуга'
        verbose_name_plural = u'Услуги'

class MainSlider(models.Model):
    title = models.CharField(max_length=200, verbose_name=u'Заголовок')
    description = HTMLField(verbose_name=u'Описание')
    image = models.ImageField(verbose_name=u'Картинка', upload_to='main-slider/', null = True,)
    priority =  models.IntegerField(default=1,blank = True, null = True, verbose_name=u'Приоритет') 

    class Meta:
        verbose_name = u'Слайд'
        verbose_name_plural = u'Слайдер на главной'

class SEO(models.Model):
    url = models.CharField(max_length=100, verbose_name=u'Относительный URL', blank=False)
    title = models.CharField(max_length=200, verbose_name=u'Заголовок страницы', blank=False)
    description = models.TextField(max_length=200, verbose_name=u'Мета-description', blank=False)
    keywords = models.CharField(max_length=200, verbose_name=u'Мета-keywords', blank=False, help_text="Ключевые слова вводить через запятую")

    class Meta:
        verbose_name = u'SEO Блок'
        verbose_name_plural = u'SEO Блок'

class WhatWeCanDo(models.Model):
    text = HTMLField(max_length=30, verbose_name=u'Текст')
    priority = models.IntegerField(default=1,blank = True, null = True, verbose_name=u'Приоритет') 

    class Meta:
        verbose_name = "Что мы можем сделать"
        verbose_name_plural = "Что мы можем сделать"

class FeedbackAboutUs(models.Model):
    author = models.CharField(max_length=30, verbose_name=u'Автор')
    about_author = HTMLField(max_length=30, verbose_name=u'Об авторе')
    feedback = HTMLField(verbose_name=u'Отзыв')

    class Meta:
        verbose_name = "Отзыв о нас"
        verbose_name_plural = "Отзывы о нас"

class News(models.Model):
    title = models.CharField(max_length=30, verbose_name=u'Заголовок')
    description = HTMLField(verbose_name=u'Краткое описание')
    text = HTMLField(verbose_name=u'Текст')
    preview = StdImageField(verbose_name=u'Картинка', upload_to='news/', null = True, blank = True, size=(120, 120, True), help_text="Не обязательно")
    publication_date = models.DateTimeField(auto_now=True, auto_now_add=True, verbose_name=u'Дата публикации')
    class Meta:
        verbose_name = "Новость"
        verbose_name_plural = "Новости"

class WhoTrustUs(models.Model):
    company_name = models.CharField(max_length=30, verbose_name=u'Название компании')
    image = StdImageField(verbose_name=u'Логотип компании', upload_to='WhoTrustUs/', null = True, blank = True, help_text="Рекомендованый размер: (218х120)", thumbnail_size=(218, 120),)
    website = models.URLField(verbose_name=u'Веб сайт компании')
    class Meta:
        verbose_name = "Кто нам доверяет"
        verbose_name_plural = "Кто нам доверяет"

class Portfolio(models.Model):
    title = models.CharField(max_length=30, verbose_name=u'Название проекта')
    work_type = models.CharField(max_length=1, verbose_name=u'Категория', choices=PORTFOLIO_TYPE_CHOICES)
    description = HTMLField(verbose_name=u'Описание проекта')
    features = HTMLField(verbose_name=u'Особенности проекта')
    website = models.URLField(verbose_name=u'Веб сайт')
    screenshot = StdImageField(verbose_name=u'Предосмотр', upload_to='portfolio/', null = True, thumbnail_size=(280, 120, True), )
    publication_date = models.DateTimeField(auto_now=True, auto_now_add=True, verbose_name=u'Дата публикации')

    def get_sub_images(self):
        return PortfolioImage.objects.filter(portfolio=self)

    class Meta:
        verbose_name = "Портфолио"
        verbose_name_plural = "Портфолио"


class PortfolioImage(models.Model):
    image = StdImageField(upload_to='portfolio/',size=(960, 960, True),)
    portfolio = models.ForeignKey(Portfolio)
    class Meta:
        verbose_name = "Изображение"
        verbose_name_plural = "Изображения"


class Technology(models.Model):
    title = models.CharField(max_length=30, verbose_name=u'Название технологии')
    image = StdImageField(verbose_name=u'Логотип технологии', upload_to='technology/', null = True, blank = True, size=(64, 64, True),)
    description = HTMLField(verbose_name=u'Описание технологии')

    class Meta:
        verbose_name = "Технология"
        verbose_name_plural = "Технологии"

class SubTechnology(models.Model):
    title = models.CharField(max_length=30, verbose_name=u'Название технологии')
    image = StdImageField(verbose_name=u'Логотип технологии', upload_to='technology/', null = True, blank = True, size=(64, 64, True),)
    description = HTMLField(verbose_name=u'Описание технологии')
    technology = models.ForeignKey(Technology, related_name = 'sub_tech')

    class Meta:
        verbose_name = "Под технология"
        verbose_name_plural = "Под технологии"


class Contacts(models.Model):
    title = models.CharField(max_length=30, verbose_name=u'Название')
    info_block1 = HTMLField(verbose_name=u'Информационный блок№1')
    info_block2 = HTMLField(verbose_name=u'Информационный блок№2')
    info_block3 = HTMLField(verbose_name=u'Информационный блок№3')
    info_block4 = HTMLField(verbose_name=u'Информационный блок№4')

    class Meta:
        verbose_name = u'Страница контакты'
        verbose_name_plural = u'Страница контакты'


class OurSkill(models.Model):
    title = models.CharField(max_length=30, verbose_name=u'Название')
    value = models.IntegerField(default=1, verbose_name=u'Число')

    class Meta:
        verbose_name = u'Навык'
        verbose_name_plural = u'Наши навыки'