# -*- coding: utf-8 -*-
from .. decorators import render_to, render_to_json

from django.template import RequestContext
from django.http import Http404,HttpResponse
from django.shortcuts import render_to_response, HttpResponseRedirect, redirect, get_object_or_404
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _
from django.utils import translation

from django.core.mail import send_mail, BadHeaderError

from django.contrib.flatpages.models import FlatPage
from django.utils.safestring import mark_safe
from newline.main.models import Portfolio, PortfolioImage, WhoTrustUs, News, FeedbackAboutUs, Technology, SubTechnology, WhatWeCanDo, MainSlider,Services, Contacts, OurSkill
from newline.main.forms import *


@render_to('home.html')
def home(request):
    wtu = WhoTrustUs.objects.all()
    wwd = WhatWeCanDo.objects.order_by('-priority')
    ms = MainSlider.objects.order_by('-priority')
    os = OurSkill.objects.order_by('-value')
    for i in range(1,len(os)):
        os[i].value = (os[i].value*100)/os[0].value
        print os[i].value
    

    f = get_object_or_404(FlatPage, url='/about/')
    f.title = mark_safe(f.title)
    f.content = mark_safe(f.content)

    return {
        'whotrustus':wtu,
        'whatwecando': wwd,
        'mainslider': ms,
        'ourskills': os,
        'flatpage': f,
	}

@render_to('portfolio/portfolio_list.html')
def portfolio(request):
    p = Portfolio.objects.order_by('-publication_date')
    #for i in range(0,len(p)):#converting select id to values
    #    p[i].work_type = p[i].get_work_type_display()
    return {
        'portfolio': p,
	}

@render_to('portfolio/portfolio_details.html')
def portfolio_details(request,page):
    p = get_object_or_404(Portfolio, id=page)
    images = p.get_sub_images
    print images
    return {
        'portfolio':p,
        'images': images,
    }

@render_to('contacts.html')
def contacts(request):
    f = get_object_or_404(Contacts)

    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            subject = cd['subject']
            from_email = cd['email']
            message = 'New feedback from newline.pro\n\n'
            message += u"Name: %s \n\n" % cd['name']
            message += "E-mail: %s \n\n" % cd['email']
            message += "Phone: %s \n\n" % cd['phone']
            message += "Message:\n"
            message += cd['message']
            try:
                send_mail(subject, message, from_email, ['alekseypaliy@gmail.com'])
            except BadHeaderError:
                raise Http404
            return {
                'ContactFormSuccess': True,
                'ContactForm': form,
                'flatpage': f,
            }
    else:
        form = ContactForm();
    return {
        'ContactForm': form,
        'flatpage': f,
    }

@render_to('blog.html')
def blog(request):
    news = News.objects.order_by('-publication_date')
    return {
        'news':news,
    }

@render_to('about.html')
def about(request):
    fau = FeedbackAboutUs.objects.all()
    wtu = WhoTrustUs.objects.all()

    f = get_object_or_404(FlatPage, url='/about/')
    f.title = mark_safe(f.title)
    f.content = mark_safe(f.content)
    return {
        'whotrustus':wtu,
        'feedbackaboutus': fau,
        'flatpage': f,
    }


@render_to('technology.html')
def technology(request):
    t = Technology.objects.all()
    
    f = get_object_or_404(FlatPage, url='/technology/')
    f.title = mark_safe(f.title)
    f.content = mark_safe(f.content)
    return {
        'technology': t,
        'flatpage': f,
    }

@render_to('services.html')
def services(request):
    s = Services.objects.order_by('-priority')
    
    f = get_object_or_404(FlatPage, url='/services/')
    f.title = mark_safe(f.title)
    f.content = mark_safe(f.content)
    return {
        'services': s,
        'flatpage': f,
    }


def lang(request, code):
    """
    Функция для смены языка с помощью ссылок.
    """
    next = request.META.get('HTTP_REFERER', '/')
    response = HttpResponseRedirect(next)
    if code and translation.check_for_language(code):
        if hasattr(request, 'session'):
            request.session['django_language'] = code
        else:
            response.set_cookie(settings.LANGUAGE_COOKIE_NAME, code)
        translation.activate(code)
    return response