# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Services'
        db.create_table(u'main_services', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('tinymce.models.HTMLField')(max_length=200)),
            ('title_ru', self.gf('tinymce.models.HTMLField')(max_length=200, null=True, blank=True)),
            ('title_en', self.gf('tinymce.models.HTMLField')(max_length=200, null=True, blank=True)),
            ('title_de', self.gf('tinymce.models.HTMLField')(max_length=200, null=True, blank=True)),
            ('description', self.gf('tinymce.models.HTMLField')()),
            ('description_ru', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('description_en', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('description_de', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('priority', self.gf('django.db.models.fields.IntegerField')(default=1, null=True, blank=True)),
        ))
        db.send_create_signal(u'main', ['Services'])

        # Adding model 'MainSlider'
        db.create_table(u'main_mainslider', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('title_ru', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('title_en', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('title_de', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('description', self.gf('tinymce.models.HTMLField')()),
            ('description_ru', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('description_en', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('description_de', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True)),
            ('priority', self.gf('django.db.models.fields.IntegerField')(default=1, null=True, blank=True)),
        ))
        db.send_create_signal(u'main', ['MainSlider'])

        # Adding model 'SEO'
        db.create_table(u'main_seo', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('url', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('title_ru', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('title_en', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('title_de', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('description', self.gf('django.db.models.fields.TextField')(max_length=200)),
            ('description_ru', self.gf('django.db.models.fields.TextField')(max_length=200, null=True, blank=True)),
            ('description_en', self.gf('django.db.models.fields.TextField')(max_length=200, null=True, blank=True)),
            ('description_de', self.gf('django.db.models.fields.TextField')(max_length=200, null=True, blank=True)),
            ('keywords', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('keywords_ru', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('keywords_en', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('keywords_de', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
        ))
        db.send_create_signal(u'main', ['SEO'])

        # Adding model 'WhatWeCanDo'
        db.create_table(u'main_whatwecando', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('text', self.gf('tinymce.models.HTMLField')(max_length=30)),
            ('text_ru', self.gf('tinymce.models.HTMLField')(max_length=30, null=True, blank=True)),
            ('text_en', self.gf('tinymce.models.HTMLField')(max_length=30, null=True, blank=True)),
            ('text_de', self.gf('tinymce.models.HTMLField')(max_length=30, null=True, blank=True)),
            ('priority', self.gf('django.db.models.fields.IntegerField')(default=1, null=True, blank=True)),
        ))
        db.send_create_signal(u'main', ['WhatWeCanDo'])

        # Adding model 'FeedbackAboutUs'
        db.create_table(u'main_feedbackaboutus', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('author', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('author_ru', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
            ('author_en', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
            ('author_de', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
            ('about_author', self.gf('tinymce.models.HTMLField')(max_length=30)),
            ('about_author_ru', self.gf('tinymce.models.HTMLField')(max_length=30, null=True, blank=True)),
            ('about_author_en', self.gf('tinymce.models.HTMLField')(max_length=30, null=True, blank=True)),
            ('about_author_de', self.gf('tinymce.models.HTMLField')(max_length=30, null=True, blank=True)),
            ('feedback', self.gf('tinymce.models.HTMLField')()),
            ('feedback_ru', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('feedback_en', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('feedback_de', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'main', ['FeedbackAboutUs'])

        # Adding model 'News'
        db.create_table(u'main_news', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('title_ru', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
            ('title_en', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
            ('title_de', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
            ('description', self.gf('tinymce.models.HTMLField')()),
            ('description_ru', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('description_en', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('description_de', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('text', self.gf('tinymce.models.HTMLField')()),
            ('text_ru', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('text_en', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('text_de', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('preview', self.gf('stdimage.fields.StdImageField')(blank=True, null=True, upload_to='news/', size={'width': 120, 'force': True, 'height': 120})),
            ('publication_date', self.gf('django.db.models.fields.DateField')(auto_now=True, auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'main', ['News'])

        # Adding model 'WhoTrustUs'
        db.create_table(u'main_whotrustus', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('company_name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('image', self.gf('stdimage.fields.StdImageField')(null=True, upload_to='WhoTrustUs/', thumbnail_size={'width': 218, 'force': None, 'height': 120}, blank=True)),
            ('website', self.gf('django.db.models.fields.URLField')(max_length=200)),
        ))
        db.send_create_signal(u'main', ['WhoTrustUs'])

        # Adding model 'Portfolio'
        db.create_table(u'main_portfolio', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('title_ru', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
            ('title_en', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
            ('title_de', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
            ('work_type', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('description', self.gf('tinymce.models.HTMLField')()),
            ('description_ru', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('description_en', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('description_de', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('features', self.gf('tinymce.models.HTMLField')()),
            ('features_ru', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('features_en', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('features_de', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('website', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('screenshot', self.gf('stdimage.fields.StdImageField')(null=True, upload_to='portfolio/', thumbnail_size={'width': 280, 'force': True, 'height': 120})),
            ('publication_date', self.gf('django.db.models.fields.DateField')(auto_now=True, auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'main', ['Portfolio'])

        # Adding model 'PortfolioImage'
        db.create_table(u'main_portfolioimage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('image', self.gf('stdimage.fields.StdImageField')(upload_to='portfolio/', size={'width': 960, 'force': True, 'height': 960})),
            ('portfolio', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.Portfolio'])),
        ))
        db.send_create_signal(u'main', ['PortfolioImage'])

        # Adding model 'Technology'
        db.create_table(u'main_technology', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('title_ru', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
            ('title_en', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
            ('title_de', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
            ('image', self.gf('stdimage.fields.StdImageField')(blank=True, null=True, upload_to='technology/', size={'width': 64, 'force': True, 'height': 64})),
            ('description', self.gf('tinymce.models.HTMLField')()),
            ('description_ru', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('description_en', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('description_de', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'main', ['Technology'])

        # Adding model 'SubTechnology'
        db.create_table(u'main_subtechnology', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('title_ru', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
            ('title_en', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
            ('title_de', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
            ('image', self.gf('stdimage.fields.StdImageField')(blank=True, null=True, upload_to='technology/', size={'width': 64, 'force': True, 'height': 64})),
            ('description', self.gf('tinymce.models.HTMLField')()),
            ('description_ru', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('description_en', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('description_de', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('technology', self.gf('django.db.models.fields.related.ForeignKey')(related_name='sub_tech', to=orm['main.Technology'])),
        ))
        db.send_create_signal(u'main', ['SubTechnology'])

        # Adding model 'Contacts'
        db.create_table(u'main_contacts', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('title_ru', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
            ('title_en', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
            ('title_de', self.gf('django.db.models.fields.CharField')(max_length=30, null=True, blank=True)),
            ('info_block1', self.gf('tinymce.models.HTMLField')()),
            ('info_block1_ru', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('info_block1_en', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('info_block1_de', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('info_block2', self.gf('tinymce.models.HTMLField')()),
            ('info_block2_ru', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('info_block2_en', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('info_block2_de', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('info_block3', self.gf('tinymce.models.HTMLField')()),
            ('info_block3_ru', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('info_block3_en', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('info_block3_de', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('info_block4', self.gf('tinymce.models.HTMLField')()),
            ('info_block4_ru', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('info_block4_en', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
            ('info_block4_de', self.gf('tinymce.models.HTMLField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'main', ['Contacts'])

        # Adding model 'OurSkill'
        db.create_table(u'main_ourskill', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('value', self.gf('django.db.models.fields.IntegerField')(default=1)),
        ))
        db.send_create_signal(u'main', ['OurSkill'])


    def backwards(self, orm):
        # Deleting model 'Services'
        db.delete_table(u'main_services')

        # Deleting model 'MainSlider'
        db.delete_table(u'main_mainslider')

        # Deleting model 'SEO'
        db.delete_table(u'main_seo')

        # Deleting model 'WhatWeCanDo'
        db.delete_table(u'main_whatwecando')

        # Deleting model 'FeedbackAboutUs'
        db.delete_table(u'main_feedbackaboutus')

        # Deleting model 'News'
        db.delete_table(u'main_news')

        # Deleting model 'WhoTrustUs'
        db.delete_table(u'main_whotrustus')

        # Deleting model 'Portfolio'
        db.delete_table(u'main_portfolio')

        # Deleting model 'PortfolioImage'
        db.delete_table(u'main_portfolioimage')

        # Deleting model 'Technology'
        db.delete_table(u'main_technology')

        # Deleting model 'SubTechnology'
        db.delete_table(u'main_subtechnology')

        # Deleting model 'Contacts'
        db.delete_table(u'main_contacts')

        # Deleting model 'OurSkill'
        db.delete_table(u'main_ourskill')


    models = {
        u'main.contacts': {
            'Meta': {'object_name': 'Contacts'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info_block1': ('tinymce.models.HTMLField', [], {}),
            'info_block1_de': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'info_block1_en': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'info_block1_ru': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'info_block2': ('tinymce.models.HTMLField', [], {}),
            'info_block2_de': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'info_block2_en': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'info_block2_ru': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'info_block3': ('tinymce.models.HTMLField', [], {}),
            'info_block3_de': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'info_block3_en': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'info_block3_ru': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'info_block4': ('tinymce.models.HTMLField', [], {}),
            'info_block4_de': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'info_block4_en': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'info_block4_ru': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'title_de': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'})
        },
        u'main.feedbackaboutus': {
            'Meta': {'object_name': 'FeedbackAboutUs'},
            'about_author': ('tinymce.models.HTMLField', [], {'max_length': '30'}),
            'about_author_de': ('tinymce.models.HTMLField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'about_author_en': ('tinymce.models.HTMLField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'about_author_ru': ('tinymce.models.HTMLField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'author': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'author_de': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'author_en': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'author_ru': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'feedback': ('tinymce.models.HTMLField', [], {}),
            'feedback_de': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'feedback_en': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'feedback_ru': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'main.mainslider': {
            'Meta': {'object_name': 'MainSlider'},
            'description': ('tinymce.models.HTMLField', [], {}),
            'description_de': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'description_en': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True'}),
            'priority': ('django.db.models.fields.IntegerField', [], {'default': '1', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'title_de': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'main.news': {
            'Meta': {'object_name': 'News'},
            'description': ('tinymce.models.HTMLField', [], {}),
            'description_de': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'description_en': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'preview': ('stdimage.fields.StdImageField', [], {'blank': 'True', 'null': 'True', 'upload_to': "'news/'", 'size': "{'width': 120, 'force': True, 'height': 120}"}),
            'publication_date': ('django.db.models.fields.DateField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'text': ('tinymce.models.HTMLField', [], {}),
            'text_de': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'text_en': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'text_ru': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'title_de': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'})
        },
        u'main.ourskill': {
            'Meta': {'object_name': 'OurSkill'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'value': ('django.db.models.fields.IntegerField', [], {'default': '1'})
        },
        u'main.portfolio': {
            'Meta': {'object_name': 'Portfolio'},
            'description': ('tinymce.models.HTMLField', [], {}),
            'description_de': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'description_en': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'features': ('tinymce.models.HTMLField', [], {}),
            'features_de': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'features_en': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'features_ru': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'publication_date': ('django.db.models.fields.DateField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'screenshot': ('stdimage.fields.StdImageField', [], {'null': 'True', 'upload_to': "'portfolio/'", 'thumbnail_size': "{'width': 280, 'force': True, 'height': 120}"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'title_de': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'website': ('django.db.models.fields.URLField', [], {'max_length': '200'}),
            'work_type': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        },
        u'main.portfolioimage': {
            'Meta': {'object_name': 'PortfolioImage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('stdimage.fields.StdImageField', [], {'upload_to': "'portfolio/'", 'size': "{'width': 960, 'force': True, 'height': 960}"}),
            'portfolio': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Portfolio']"})
        },
        u'main.seo': {
            'Meta': {'object_name': 'SEO'},
            'description': ('django.db.models.fields.TextField', [], {'max_length': '200'}),
            'description_de': ('django.db.models.fields.TextField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'description_en': ('django.db.models.fields.TextField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'description_ru': ('django.db.models.fields.TextField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'keywords': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'keywords_de': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'keywords_en': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'keywords_ru': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'title_de': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'main.services': {
            'Meta': {'object_name': 'Services'},
            'description': ('tinymce.models.HTMLField', [], {}),
            'description_de': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'description_en': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'priority': ('django.db.models.fields.IntegerField', [], {'default': '1', 'null': 'True', 'blank': 'True'}),
            'title': ('tinymce.models.HTMLField', [], {'max_length': '200'}),
            'title_de': ('tinymce.models.HTMLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'title_en': ('tinymce.models.HTMLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('tinymce.models.HTMLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'main.subtechnology': {
            'Meta': {'object_name': 'SubTechnology'},
            'description': ('tinymce.models.HTMLField', [], {}),
            'description_de': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'description_en': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('stdimage.fields.StdImageField', [], {'blank': 'True', 'null': 'True', 'upload_to': "'technology/'", 'size': "{'width': 64, 'force': True, 'height': 64}"}),
            'technology': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'sub_tech'", 'to': u"orm['main.Technology']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'title_de': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'})
        },
        u'main.technology': {
            'Meta': {'object_name': 'Technology'},
            'description': ('tinymce.models.HTMLField', [], {}),
            'description_de': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'description_en': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('tinymce.models.HTMLField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('stdimage.fields.StdImageField', [], {'blank': 'True', 'null': 'True', 'upload_to': "'technology/'", 'size': "{'width': 64, 'force': True, 'height': 64}"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'title_de': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'})
        },
        u'main.whatwecando': {
            'Meta': {'object_name': 'WhatWeCanDo'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'priority': ('django.db.models.fields.IntegerField', [], {'default': '1', 'null': 'True', 'blank': 'True'}),
            'text': ('tinymce.models.HTMLField', [], {'max_length': '30'}),
            'text_de': ('tinymce.models.HTMLField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'text_en': ('tinymce.models.HTMLField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'text_ru': ('tinymce.models.HTMLField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'})
        },
        u'main.whotrustus': {
            'Meta': {'object_name': 'WhoTrustUs'},
            'company_name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('stdimage.fields.StdImageField', [], {'null': 'True', 'upload_to': "'WhoTrustUs/'", 'thumbnail_size': "{'width': 218, 'force': None, 'height': 120}", 'blank': 'True'}),
            'website': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['main']