from django.contrib import admin
from modeltranslation.admin import TranslationAdmin, TranslationStackedInline
from newline.main.models import Portfolio, PortfolioImage, WhoTrustUs, News, FeedbackAboutUs, Technology, SubTechnology, WhatWeCanDo, SEO, MainSlider, Services, Contacts, OurSkill

from django.contrib.flatpages.models import FlatPage
from django.contrib.flatpages.admin import FlatpageForm, FlatPageAdmin
from django import forms
from tinymce.widgets import TinyMCE
from django.core.urlresolvers import reverse

class OurSkillAdmin(admin.ModelAdmin):
    list_display = ('title','value')

class ContactsAdmin(TranslationAdmin):
    list_display = ('title',)

class ServicesAdmin(TranslationAdmin):
    list_display = ('title','priority')

class MainSliderAdmin(TranslationAdmin):
    list_display = ('title','priority')

class SEOAdmin(TranslationAdmin):
    list_display = ('title', 'url')

class WhatWeCanDoAdmin(TranslationAdmin):
    list_display = ('text',)

class FeedbackAboutUsAdmin(TranslationAdmin):
    list_display = ('author', 'about_author')
    search_fields = ('author',)

class NewsAdmin(TranslationAdmin):
    list_display = ('title', 'publication_date')
    search_fields = ('title',)
    list_filter = ('publication_date',)

class MyFlatPageAdmin(FlatPageAdmin, TranslationAdmin):

    def formfield_for_dbfield(self, db_field, **kwargs):
        field = super(MyFlatPageAdmin, self).formfield_for_dbfield(db_field, **kwargs)
        if db_field.name == 'content':
            field = db_field.formfield(widget= TinyMCE(attrs= {'cols': 80, 'rows': 30}, mce_attrs= {'external_link_list_url': reverse('tinymce.views.flatpages_link_list')}))
        self.patch_translation_field(db_field, field, **kwargs)
        return field

class WhoTrustUsAdmin(admin.ModelAdmin):
    list_display = ('company_name', 'website')
    search_fields = ('company_name', 'website')

class PortfolioImageInline(admin.TabularInline):
    model = PortfolioImage

class PortfolioAdmin(TranslationAdmin):
    list_display = ('title', 'website','publication_date')
    search_fields = ('title', 'website')
    list_filter = ('publication_date',)
    inlines = [PortfolioImageInline,]

class SubTechnologyInline(TranslationStackedInline):
    model = SubTechnology

class TechnologyAdmin(TranslationAdmin):
    list_display = ('title',)
    search_fields = ('title',)
    inlines = [SubTechnologyInline,]

admin.site.register(OurSkill,OurSkillAdmin)
admin.site.register(Contacts,ContactsAdmin)
admin.site.register(Services,ServicesAdmin)
admin.site.register(MainSlider,MainSliderAdmin)
admin.site.register(SEO,SEOAdmin)
admin.site.register(WhatWeCanDo,WhatWeCanDoAdmin)
admin.site.register(Technology,TechnologyAdmin)
admin.site.register(FeedbackAboutUs,FeedbackAboutUsAdmin)
admin.site.register(News,NewsAdmin)
admin.site.register(Portfolio,PortfolioAdmin)
admin.site.register(WhoTrustUs,WhoTrustUsAdmin)
admin.site.unregister(FlatPage)
admin.site.register(FlatPage, MyFlatPageAdmin)