Welcome to NewLine site repository
------------------------------------

Чтобы запустить сайт:

1. Скачиваем его git clone https://github.com/Arti3DPlayer/NewLine.git

2. Создаём виртуальное окружение virtualenv env

3. Активируем его . /env/bin/activate

4. Ставим пакеты pip install -r requirements.txt

5. Создаем бд, прописываем имя и пароль в settings. Выполняем команду python manage.py syncdb

6. Запускаем python manage.py runserver


Good luck & have fun


Основные команды Git

git status
git add
git commit -a
git push origin ветка
